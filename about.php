<!doctype html>
<html class=no-js lang="">
   <!-- Mirrored from 0effortthemes.com/soccerclubv2/about.php by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 28 May 2019 11:55:09 GMT -->
   <!-- Added by HTTrack -->
   <meta http-equiv="content-type" content="text/html;charset=utf-8" />
   <!-- /Added by HTTrack -->
   <head>
      <meta charset=utf-8>
      <meta name=description content="">
      <meta name=viewport content="width=device-width, initial-scale=1">
      <title>Team Rebound</title>
      <link rel="shortcut icon" href=favicon.ico>
      <link rel=stylesheet href=vendor.css>
      <link rel=stylesheet href=style.css>
      <link rel=stylesheet type=text/css href=css/layerslider.css>
      <script src=js/vendor/modernizr.js></script>
   </head>
   <body>
      <!--[if lt IE 10]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
      <![endif]-->
      <div class=wrapper>
         <header class=header-main>
            
            <div class="header-lower clearfix">
               <div class=container>
                  <div class=row>
                     <h1 class=logo><a href=index.php><img src=images/logo.png alt=image></a></h1>
                     <div class=menubar>
                        <nav class=navbar>
                           <div class=nav-wrapper>
                              <div class=navbar-header><button type=button class=navbar-toggle><span class=sr-only>Toggle navigation</span> <span class=icon-bar></span></button></div>
                              <div class=nav-menu>
                                 <ul class="nav navbar-nav menu-bar">
                                    <li><a href=index.php >Home</a></li>
                                    <li><a href=about.php class=active>about Us</a></li>
                                    <li><a href=gallery.php>Gallery</a></li>
                                    <li><a href=news.php>News & Events</a></li>
                                    <li><a href=contact.php>Contact Us</a></li>
                                    <li><a href=signup.php>Registration</a></li>
                                    <li><a href=login.php>Login</a></li>
                                 </ul>
                              </div>
                           </div>
                        </nav>
                     </div>
                     <div class=social><a href=https://www.facebook.com/ class=facebook><i class="fa fa-facebook"></i></a> <a href=https://twitter.com/ class=twitter><i class="fa fa-twitter"></i></a></div>
                  </div>
               </div>
            </div>
         </header>
         <div class=innerbannerwrap>
            <div class=innerbanner1>
               <h2 class=bannerHeadline>about <span>us</span></h2>
            </div>
         </div>
         <section class="matchSchedule countryclub">
            <div class=container>
               <div class=row>
                  <h2 class="heading small">team <span>rebound</span></h2>
                  <p class="headParagraph mar">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil debitis mollitia qui libero voluptate ratione, molestias! Necessitatibus voluptatem temporibus doloremque non, vel ipsam, nesciunt dolores consequatur, est tempora ut! Est?>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil debitis mollitia qui libero voluptate ratione, molestias! Necessitatibus voluptatem temporibus doloremque non, vel ipsam, nesciunt dolores consequatur, est tempora ut! Est.</p>
               </div>
            </div>
         </section>
         <section class=players>
            <div class=container>
               <div class=row>
                  <h2 class=heading>board <span>members</span></h2>
                  <p class=headParagraph>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis consequuntur animi, commodi, voluptatibus labore aperiam fugit maxime quos optio architecto cum? Laborum nesciunt doloribus expedita atque error rem molestias, ducimus.</p>
                  <div class="wrapplayer clearfix">
                     <a class="prv prev-player"></a> <a class="nxt next-player"></a>
                     <ul class="boardmember clearfix">
                        <li class=clearfix>
                           <div>
                              <div>
                                 <div class=fig01>
                                    <div class=memberimg style="background:url(images/boardMember/boardMember01.jpg) no-repeat top center"></div>
                                 </div>
                                 <div class="bg-black01 fig02">
                                    <h6 class=paragraph02>Sammie Stoddard</h6>
                                    <p class="uppercaseheading red">director</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam officia labore molestias, Doloribus, aspernatur.</p>
                                 </div>
                                 <div class="bg-black fig02">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit repellat cum necessitatibus quas magni! Qui, modi possimus.</p>
                                 </div>
                                 <div class="bg-redcolor fig02">
                                    <p>BORN 14 April 1975, England</p>
                                 </div>
                              </div>
                           </div>
                        </li>
                        <li>
                           <div>
                              <div>
                                 <div class=fig01>
                                    <div class=memberimg style="background:url(images/boardMember/boardMember02.jpg) no-repeat top center"></div>
                                 </div>
                                 <div class="bg-black01 fig02">
                                    <h6 class=paragraph02>Dominick Dumbleton</h6>
                                    <p class="uppercaseheading red">ceo</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam officia labore molestias, Doloribus, aspernatur.</p>
                                 </div>
                                 <div class="bg-black fig02">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit repellat cum necessitatibus quas magni! Qui, modi possimus.</p>
                                 </div>
                                 <div class="bg-redcolor fig02">
                                    <p>BORN 14 April 1975, England</p>
                                 </div>
                              </div>
                           </div>
                        </li>
                        <li>
                           <div>
                              <div>
                                 <div class=fig01>
                                    <div class=memberimg style="background:url(images/boardMember/boardMember03.jpg) no-repeat top center"></div>
                                 </div>
                                 <div class="bg-black01 fig02">
                                    <h6 class=paragraph02>Leland Lagos</h6>
                                    <p class="uppercaseheading red">ceo</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam officia labore molestias, Doloribus, aspernatur.</p>
                                 </div>
                                 <div class="bg-black fig02">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit repellat cum necessitatibus quas magni! Qui, modi possimus.</p>
                                 </div>
                                 <div class="bg-redcolor fig02">
                                    <p>BORN 14 April 1975, England</p>
                                 </div>
                              </div>
                           </div>
                        </li>
                        <li>
                           <div>
                              <div>
                                 <div class=fig01>
                                    <div class=memberimg style="background:url(images/boardMember/boardMember02.jpg) no-repeat top center"></div>
                                 </div>
                                 <div class="bg-black01 fig02">
                                    <h6 class=paragraph02>Kenton Kidd</h6>
                                    <p class="uppercaseheading red">ceo</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam officia labore molestias, Doloribus, aspernatur.</p>
                                 </div>
                                 <div class="bg-black fig02">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit repellat cum necessitatibus quas magni! Qui, modi possimus.</p>
                                 </div>
                                 <div class="bg-redcolor fig02">
                                    <p>BORN 14 April 1975, England</p>
                                 </div>
                              </div>
                           </div>
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </section>
         <section class=club_history>
            <div class=container>
               <div class=row>
                  <h2 class=heading>club <span>history</span></h2>
                  <p class=headParagraph>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis consequuntur animi, commodi, voluptatibus labore aperiam fugit maxime quos optio architecto cum? Laborum nesciunt doloribus expedita atque error rem molestias, ducimus.</p>
                  <div class="nav clubHistory-wrap clearfix">
                     <ul class=historyMeter>
                        <li><a href=#><span>1987</span></a></li>
                        <li class=win><a href=#win01><span>1988</span></a></li>
                        <li><a href=#><span>1989</span></a></li>
                        <li><a href=#><span>1990</span></a></li>
                        <li class="win active"><a href=#win02><span>1991</span></a></li>
                        <li><a href=#><span>1991</span></a></li>
                        <li><a href=#><span>1995</span></a></li>
                        <li><a href=#><span>1997</span></a></li>
                        <li class=win><a href=#win03><span>2000</span></a></li>
                        <li><span>2005</span></li>
                        <li><span>2007</span></li>
                        <li><span>2009</span></li>
                        <li class=win><a href=#win04><span>2012</span></a></li>
                        <li><a href=#><span>2013</span></a></li>
                        <li><a href=#><span>2014</span></a></li>
                        <li class=win><a href=#win05><span>2015</span></a></li>
                     </ul>
                     <div class="tab-content historyVideoWrap clearfix">
                        <div role=tabpanel class="tab-pane clearfix" id=win01>
                           <div class=historyVideo>
                              <div class=historyvideoContainer><iframe src=https://www.youtube.com/embed/YYUy4pF2eto allowfullscreen=""></iframe></div>
                           </div>
                           <div class=historyContent>
                              <h4><span>2019 Basketball World Cup</span> champions league</h4>
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam non voluptatibus error a esse maiores, ducimus sit unde alias aspernatur perspiciatis itaque corporis? Accusamus pariatur dolorum repellendus consectetur tempore harum?</p>
                              <div class="blog-detailsfooter clubfooter clearfix">
                                 <div class="blog-detailsfooter01 clearfix">
                                    <p class=uppercaseheading>octomber 14 ,<span class=red>2015</span></p>
                                 </div>
                                 <div class=blog-detailsfooter02><a href=# class="social_link facebook"><i class="fa fa-facebook"></i></a> <a href=# class="social_link twitter"><i class="fa fa-twitter"></i></a> <a href=# class="social_link behance"><i class="fa fa-behance"></i></a></div>
                                 <div></div>
                              </div>
                           </div>
                        </div>
                        <div role=tabpanel class="tab-pane active clearfix" id=win02>
                           <div class=historyVideo>
                              <div class=historyvideoContainer><iframe src=https://www.youtube.com/embed/YYUy4pF2eto allowfullscreen=""></iframe></div>
                           </div>
                           <div class=historyContent>
                              <h4><span>2019 Basketball World Cup</span> champions league</h4>
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam non voluptatibus error a esse maiores, ducimus sit unde alias aspernatur perspiciatis itaque corporis? Accusamus pariatur dolorum repellendus consectetur tempore harum?</p>
                              <div class="blog-detailsfooter clubfooter clearfix">
                                 <div class="blog-detailsfooter01 clearfix">
                                    <p class=uppercaseheading>octomber 14 ,<span class=red>2015</span></p>
                                 </div>
                                 <div class=blog-detailsfooter02><a href=# class="social_link facebook"><i class="fa fa-facebook"></i></a> <a href=# class="social_link twitter"><i class="fa fa-twitter"></i></a> <a href=# class="social_link behance"><i class="fa fa-behance"></i></a></div>
                                 <div></div>
                              </div>
                           </div>
                        </div>
                        <div role=tabpanel class="tab-pane clearfix" id=win03>
                           <div class=historyVideo>
                              <div class=historyvideoContainer><iframe src=https://www.youtube.com/embed/YYUy4pF2eto allowfullscreen=""></iframe></div>
                           </div>
                           <div class=historyContent>
                              <h4><span>2019 Basketball World Cup</span> champions league</h4>
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam non voluptatibus error a esse maiores, ducimus sit unde alias aspernatur perspiciatis itaque corporis? Accusamus pariatur dolorum repellendus consectetur tempore harum?</p>
                              <div class="blog-detailsfooter clubfooter clearfix">
                                 <div class="blog-detailsfooter01 clearfix">
                                    <p class=uppercaseheading>octomber 14 ,<span class=red>2015</span></p>
                                 </div>
                                 <div class=blog-detailsfooter02><a href=# class="social_link facebook"><i class="fa fa-facebook"></i></a> <a href=# class="social_link twitter"><i class="fa fa-twitter"></i></a> <a href=# class="social_link behance"><i class="fa fa-behance"></i></a></div>
                                 <div></div>
                              </div>
                           </div>
                        </div>
                        <div role=tabpanel class="tab-pane clearfix" id=win04>
                           <div class=historyVideo>
                              <div class=historyvideoContainer><iframe src=https://www.youtube.com/embed/YYUy4pF2eto allowfullscreen=""></iframe></div>
                           </div>
                           <div class=historyContent>
                              <h4><span>2019 Basketball World Cup</span> champions league</h4>
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam non voluptatibus error a esse maiores, ducimus sit unde alias aspernatur perspiciatis itaque corporis? Accusamus pariatur dolorum repellendus consectetur tempore harum?</p>
                              <div class="blog-detailsfooter clubfooter clearfix">
                                 <div class="blog-detailsfooter01 clearfix">
                                    <p class=uppercaseheading>octomber 14 ,<span class=red>2015</span></p>
                                 </div>
                                 <div class=blog-detailsfooter02><a href=# class="social_link facebook"><i class="fa fa-facebook"></i></a> <a href=# class="social_link twitter"><i class="fa fa-twitter"></i></a> <a href=# class="social_link behance"><i class="fa fa-behance"></i></a></div>
                                 <div></div>
                              </div>
                           </div>
                        </div>
                        <div role=tabpanel class="tab-pane clearfix" id=win05>
                           <div class=historyVideo>
                              <div class=historyvideoContainer><iframe src=https://www.youtube.com/embed/YYUy4pF2eto allowfullscreen=""></iframe></div>
                           </div>
                           <div class=historyContent>
                              <h4><span>2019 Basketball World Cup</span> champions league</h4>
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam non voluptatibus error a esse maiores, ducimus sit unde alias aspernatur perspiciatis itaque corporis? Accusamus pariatur dolorum repellendus consectetur tempore harum?</p>
                              <div class="blog-detailsfooter clubfooter clearfix">
                                 <div class="blog-detailsfooter01 clearfix">
                                    <p class=uppercaseheading>octomber 14 ,<span class=red>2015</span></p>
                                 </div>
                                 <div class=blog-detailsfooter02><a href=# class="social_link facebook"><i class="fa fa-facebook"></i></a> <a href=# class="social_link twitter"><i class="fa fa-twitter"></i></a> <a href=# class="social_link behance"><i class="fa fa-behance"></i></a></div>
                                 <div></div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         
         <section class="sponsors bg-smallwhite">
            <div class=container>
               <div class=row>
                  <h2 class=heading>spon<span>sors</span></h2>
                  <ul class="client clearfix">
                     <li><a href="#"><img src=images/client/client01.png alt=image></a></li>
                     <li><a href="#"><img src=images/client/client02.png alt=image></a></li>
                     <li><a href="#"><img src=images/client/client03.png alt=image></a></li>
                     <li><a href="#"><img src=images/client/client04.png alt=image></a></li>
                     <li><a href="#"><img src=images/client/client02.png alt=image></a></li>
                     <li><a href="#"><img src=images/client/client05.png alt=image></a></li>
                  </ul>
               </div>
            </div>
         </section>
         <footer>
            
            <div class=footer-type02>
               <div class=container>
                  <div class=row>
                     <a href=index.php class=footer-logo><img src=images/logo.png alt=image></a>
                     <div class=footer-container>
                        <ul class=clearfix>
                           <li><a href=https://www.facebook.com/ class=bigsocial-link><i class="fa fa-facebook"></i></a></li>
                           <li><a href=https://twitter.com/0effortthemes class=bigsocial-link target=_blank><i class="fa fa-twitter"></i></a></li>
                           <li><a href=https://www.behance.net/itobuz class=bigsocial-link target=_blank><i class="fa fa-behance"></i></a></li>
                        </ul>
                        <p>Copyright, 2019 teamrebound.org. Designed by <a href="https://tahcom.com/" class=copyright target="_blank" >Tahcom</a></p>
                     </div>
                     <div class=footer-appstore>
                        <figure><a href=#><img src=images/appstore/apple.png alt=image></a></figure>
                        <figure><a href=#><img src=images/appstore/google.png alt=image></a></figure>
                     </div>
                  </div>
               </div>
            </div>
         </footer>
      </div>
      <script src=js/vendor/vendor.js></script><script src=js/main.js></script>
   </body>
   <!-- Mirrored from 0effortthemes.com/soccerclubv2/about.php by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 28 May 2019 11:55:35 GMT -->
</html>