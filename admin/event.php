<!DOCTYPE html>
<?php include("../connection.php"); ?>
<?php
if(!isset($_SESSION['admin']))
{

  header("location: ../dash/log.php");
}
?>
<html class="loading" lang="en" data-textdirection="ltr">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content=".">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <title>Team Rebound</title>
    <link rel="apple-touch-icon" href="app-assets/images/logo/logo.png">
     <link rel="shortcut icon" href=../favicon.ico>
    <link href="https://fonts.googleapis.com/css?family=Muli:300,300i,400,400i,600,600i,700,700i%7CComfortaa:300,400,700" rel="stylesheet">
    <link href="app-assets/fonts/line-awesome/css/line-awesome.min.css" rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/forms/toggle/switchery.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/forms/switch.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/colors/palette-switch.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/charts/chartist.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/charts/chartist-plugin-tooltip.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN CHAMELEON  CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/app.min.css">
    <!-- END CHAMELEON  CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/vertical-menu.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/colors/palette-gradient.min.css">
    <!-- <link rel="stylesheet" type="text/css" href="app-assets/css/core/colors/palette-gradient.min.css"> -->
    <link rel="stylesheet" type="text/css" href="app-assets/css/pages/chat-application.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/pages/dashboard-analytics.min.css">
     <link href="assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css">
      <link href="assets/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css">
      <link href="assets/plugins/jvectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet">

    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
     <script type="text/javascript" src="typeahead.js"></script>
   <style>
   .typeahead { border: 2px solid #FFF;border-radius: 4px;padding: 8px 12px;max-width: 300px;min-width: 290px;background: rgba(66, 52, 52, 0.5);color: #FFF;}
   .tt-menu { width:300px; }
   ul.typeahead{margin:0px;padding:10px 0px;


    width: 97%;
    max-width: none;}

ul.typeahead.dropdown-menu li.active    { background: #ddd !important; }

   ul.typeahead.dropdown-menu li a {padding: 10px !important;  border-bottom:#CCC 1px solid;color:#FFF;}
   ul.typeahead.dropdown-menu li:last-child a { border-bottom:0px !important; }
   .bgcolor {max-width: 550px;min-width: 290px;max-height:340px;background:url("world-contries.jpg") no-repeat center center;padding: 100px 10px 130px;border-radius:4px;text-align:center;margin:10px;}
   .demo-label {font-size:1.5em;color: #686868;font-weight: 500;color:#FFF;}
   /*.dropdown-menu>.active>a, .dropdown-menu>.active>a:focus, .dropdown-menu>.active>a:hover {
      text-decoration: none;
      background-color: #8e71b1;
      outline: 0;
   }*/
   </style>


   <script>
    $(document).ready(function () {
      
        $('#txt1').typeahead({

            source: function (query, result) {
                $.ajax({
                    url: "server.php",
               data: 'query=' + query,            
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                  result($.map(data, function (item) {
                     return item;
                        }));
                    }
                });
            }
        });
    });
</script>
    <!-- END Custom CSS-->
    <script type="text/javascript">
        function delete_id(id)
{
     if(confirm('Sure To Remove This Details ?'))
     {
        window.location.href='event.php?delete_id='+id;
     }
}



</script>
    
</head>

<body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-color="bg-gradient-x-purple-blue" data-col="2-columns">
<?php
if(isset($_REQUEST['delete_id']))
{
 $sql_query="DELETE FROM tbl_event WHERE id=".$_REQUEST['delete_id'];
 
     mysql_query($sql_query);
     header("Location: event.php");
}
if(isset($_REQUEST['deletephoto_id']))
{
 $sql_query="DELETE FROM tbl_eventphoto WHERE id=".$_REQUEST['deletephoto_id'];
 
     mysql_query($sql_query);
     header("Location: event.php");
}
?>
     <nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-semi-light">
        <div class="navbar-wrapper">
            <div class="navbar-container content">
                <div class="collapse navbar-collapse show" id="navbar-mobile">
                    <ul class="nav navbar-nav mr-auto float-left">
                        <li class="nav-item mobile-menu d-md-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
                        <li class="nav-item d-none d-md-block"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu"></i></a></li>
                    </ul>
                    <ul class="nav navbar-nav float-right">



                        <li class="dropdown dropdown-user nav-item">
                            <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown"> <span class="avatar avatar-online"><img src="1.jpg" alt="avatar"></span></a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <div class="arrow_box_right"><a class="dropdown-item" href="#"><span class="avatar avatar-online"><img src="../images/logo.png" alt="avatar"><span class="user-name text-bold-700 ml-1">Admin</span></span></a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="pass.php"><i class="ft-unlock"></i> Change Password</a>
                                    <div class="dropdown-divider"></div><a class="dropdown-item" href="../dash/log.php"><i class="ft-power"></i> Logout</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>

    <div class="main-menu menu-fixed menu-light menu-accordion    menu-shadow " data-scroll-to-active="true" data-img="app-assets/images/backgrounds/02.jpg">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mr-auto">
                    <a class="navbar-brand" href="index.php"><img class="brand-logo" alt="Chameleon admin logo" src="../images/logo.png" />
                        <h3 class="brand-text">Team Rebound</h3>
                    </a>
                </li>
                <li class="nav-item d-md-none"><a class="nav-link close-navbar"><i class="ft-x"></i></a></li>
            </ul>
        </div>
        <div class="main-menu-content">
            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                 <li class="nav-item "><a href="index.php"><i class="ft-image"></i><span class="menu-title" data-i18n="">Home</span></a>
                </li><li class="nav-item "><a href="event.php"><i class="ft-image"></i><span class="menu-title" data-i18n="">Event</span></a>
                </li>
                  <!--   <ul class="menu-content">
                        <li class="nav-item active"><a href="banner.php"><span class="menu-title" data-i18n="">Banner</span></a>
                        </li>
                        <li class="nav-item "><a href="home-about-welcome.php"><span class="menu-title" data-i18n="">Home welcome </span></a>
                        </li>
                        <li class="nav-item "><a href="specialization.php"><span class="menu-title" data-i18n="">Specialization</span></a>
                        </li>
                        
                         <li class="nav-item "><a href="latest-projects.php"><span class="menu-title" data-i18n="">Latest Projects</span></a>
                        </li>
                        
                         
                         <li class="nav-item "><a href="features.php"><span class="menu-title" data-i18n="">Features</span></a>
                        </li>
                        
                        
                             
                         <li class="nav-item "><a href="testimonials.php"><span class="menu-title" data-i18n="">Testimonials</span></a>
                        </li>



                    </ul> -->
                </li>
                <li class="nav-item "><a href="#"><i class="ft-info"></i><span class="menu-title" data-i18n="">View </span></a>
                
                  <ul class="menu-content">
                     <li class="nav-item "><a href="member.php"><span class="menu-title" data-i18n="">   Members</span></a>
                        </li>
                        
                        <li class="nav-item "><a href="public.php"><span class="menu-title" data-i18n="">participants</span></a>
                        </li>
                        
                        
                         <!--  <li class="nav-item "><a href="areas-of-work.php"><span class="menu-title" data-i18n="">Areas Of Work</span></a>
                        </li> -->



                  </ul>
                
                </li>

<!-- 
                <li class="nav-item "><a href="project-detail.php"><i class="ft-image"></i><span class="menu-title" data-i18n="">Projects</span></a>
                </li>
                <li class="nav-item "><a href="services.php"><i class="ft-disc"></i><span class="menu-title" data-i18n="">Services</span></a>
                </li>
                
                <li class="nav-item "><a href="contact.php"><i class="ft-phone"></i><span class="menu-title" data-i18n="">Contact Us</span></a>
                </li> -->
                
            </ul>
        </div>
        <div class="navigation-background"></div>
    </div>

    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-wrapper-before"></div>
            <div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title">Member List</h3>
                </div>
            </div>
           <div class="page-wrapper">
       
         <!--end page-wrapper-inner --><!-- Page Content-->
         <div class="page-content">
            <div class="container-fluid">


                       <div class="row match-height">


                                <div class="col-md-6">
                                   <div class="card">
                        <div class="card-header">
                                            <h2>event-Category </h2>

                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="card-content">
                                            <br>
                                            <form class="form-horizontal form-label-left input_mask" method="post" enctype="multipart/form-data">
                                               <div class="form-group">
                                                    <!-- <label for="">Experience Of Company</label> -->
                                                    <input type="text" class="form-control" id="t1" name="t1" placeholder="Category ">
                                                   
                                                </div>


                                           <!--  -->


                                                <div class="form-group">
                                                  

                                                   
                                                        <input type="submit" name="b1" value="Add" class="btn btn-success">
                                                   
                                                </div>




<?php
if(isset($_POST['b1']))
{
    $t1=$_POST['t1'];
    
    $d=date("d/m/y");

       

$res=mysql_query("insert into  tbl_eventcategory values('$t1','$d','0')");

msg("Successfully Uploaded","event.php");


}





if(isset($_REQUEST['delete_idcat']))
{
 // echo "dsfk ds";
 $sql_query="DELETE FROM tbl_eventcategory WHERE id=".$_REQUEST[' '];
  // echo $sql_query;
     mysql_query($sql_query);


      // echo "dsfk ds"; 
 $sql_query="DELETE FROM tbl_event WHERE pid=".$_REQUEST['delete_idcat'];
  // echo $sql_query;
     mysql_query($sql_query);
     header("Location: event.php");
}



?>







                                            </form>
                                        </div>
                                    </div>
                                </div>




         <div class="col-md-6">
                         <div class="card">
                        <div class="card-header">
                                <h2>View Category</h2>

                                <div class="clearfix"></div>
                            </div>
                              <form id="demo-form3" data-parsley-validate="" class="" novalidate="" method="post">
                            <div class="card-content">

                                 <div class="table-responsive dash-social">
                              <table id="datatable" class="table table-bordered">
 
  <?php
$res=mysql_query("select * from  tbl_eventcategory order by id desc");
$i=0;
while($row=mysql_fetch_row($res))
{


    ?>
<tr>
                                            <td>    <?php
echo $row[0];
    
    
    ?></td>

                               
                                    


                         <td>                <a href="javascript:delete_idcat(<?php echo $row[2]; ?>)"  class="close"  aria-label="Close">              <span aria-hidden="true" title="delete">×</span></a></td>



                                  
                                </tr>

                                <?php
                                }
                                ?>

</table>
</div>

                            </div>

                        </div>
                    </form>
                    </div>
















</div>








              
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title" id="basic-layout-form">Upload Latest Events</h4>
                            <a class="heading-elements-toggle">
                                <i class="la la-ellipsis-v font-medium-3"></i>
                            </a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li>
                                        <a data-action="collapse">
                                            <i class="ft-minus"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a data-action="reload">
                                            <i class="ft-rotate-cw"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a data-action="expand">
                                            <i class="ft-maximize"></i>
                                        </a>
                                    </li>

                                </ul>
                            </div>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                           <form class="form-horizontal form-label-left input_mask" method="post" enctype="multipart/form-data">
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-12">
<div class="form-group">
                                                    <label for="t2">Event Category </label>

                                                   <select class="form-control" id="s1" name="s1" required>
    <option>--Select Event Category--</option>
    <?php 
$qry5="select * from tbl_event_category";
$res5=mysql_query($qry5);

while($row5=mysql_fetch_row($res5))
{

    echo "<option value=$row5[2]>$row5[0]</option>";

}
    ?>
}


</select>
                                                </div>    
                                             

                                                 <div class="form-group">
                                                    <label for="">Upload Event Photo</label>
                                                    <div class="custom-file">
                                                        <input type="file" class="custom-file-input" id="f1" name="f1" accept="image/*" required>
                                                        <label class="custom-file-label" for="f1">Choose file</label>
                                                    </div>
                                                </div>
<div class="form-group">
                                                    <label for="">Upload Related Photos</label>
                                                    <div class="custom-file">
                                                        <input type="file" class="custom-file-input" id="f2[]"  name="f2[]" multiple accept="image/*" required>
                                                        <label class="custom-file-label" for="f1">Choose file</label>
                                                    </div>
                                                </div>

<div class="form-group">
                                                    <label for="t2">Event Title </label>

                                                    <input type="text" id="t1" class="form-control" placeholder="Title " name="t1">
                                                </div>                                                
                   <div class="form-group">
                                                    <label for="t2">Event Venue Address</label>
<textarea id="t1" class="form-control" placeholder="Venue Address" name="t2"></textarea>
                                                   
                                                </div>                                      
  <div class="form-group">
                                                    <label for="t3">Event Description (Para 1)</label>
<textarea id="t3" class="form-control" placeholder="Event Description" name="t3"></textarea>
                                                   
                                                </div>
                                                <div class="form-group">
                                                    <label for="t3">Event Description (Para 2)</label>
<textarea id="t3" class="form-control" placeholder="Event Description" name="t3"></textarea>
                                                   
                                                </div>
                                                <div class="form-group">
                                                    <label for="t3">Event Description (Para 3)</label>
<textarea id="t3" class="form-control" placeholder="Event Description" name="t3"></textarea>
                                                   
                                                </div>

<div class="form-group">
                                                    <label for="t2">Event Date </label>

                                                    <input type="date" id="t4" class="form-control" placeholder="Title " name="t4">
                                                </div>                                                
                                           
                               
<div class="form-group">
                                                    <label for="t2">Event Time </label>

                                                    <input type="text" id="t4" class="form-control" placeholder="Title " name="t4">
                                                </div>      
                                                          <div class="form-group">
                                                    <label for="t2">You tube link </label>

                                                    <input type="text" id="t4" class="form-control" placeholder="Title " name="t4">
                                                </div> 
                                                <div class="form-actions">

                                                    <input  name="b1" type="submit" class="btn btn-primary" value="Upload">
                                                               
                                                </div>
                                            </div>

                                        </div>

                                    </div>
<?php
if(isset($_POST['b4']))
{

$d=date("d/m/y");


$t2=$_POST['t2'];
$t3=$_POST['t3'];
$t4=$_POST['t4'];
$t5=$_POST['t5'];
$name=$_FILES['f1']['name'];
$maxid=$_SESSION['eventid'];
if($name=="")
{
mysql_query("UPDATE `tbl_event` SET `title` = '$t2', `address` = '$t3', `des` = '$t4', `edate` = '$t5', `pdate` = '$d' WHERE `tbl_event`.`id` = '$maxid'");
if(mysql_affected_rows()>0)
{
msg("Event Updation Completed","event.php");

}
else
{
    
   msg("Error","event.php"); 
}
}
else
{

$ext = end((explode(".", $name))); # extra () to prevent notice
$cover =  $maxid.".".$ext;



  //move_uploaded_file($_FILES['f1']['tmp_name'],'../projectcoverphoto/'.$cover);
//     $ext = end((explode(".", $name))); # extra () to prevent notice
// $filename =  $id.".".$ext;

 $url ="../event/$cover";
 move_uploaded_file($_FILES["f1"]["tmp_name"], $url);
mysql_query("UPDATE `tbl_event` SET photo='$cover',`title` = '$t2', `address` = '$t3', `des` = '$t4', `edate` = '$t5', `pdate` = '$d' WHERE `tbl_event`.`id` = '$maxid'");
if(mysql_affected_rows()>0)
{
msg("Event Updation Completed","event.php");
}
else
{
    
   msg("Error","event.php"); 
}
}
}
if(isset($_POST['b1']))
{

$d=date("d/m/y");

$t1=$_POST['t1'];
$t2=$_POST['t2'];
$t3=$_POST['t3'];
$t4=$_POST['t4'];
$name=$_FILES['f1']['name'];

$res1=mysql_query("select max(id) as id from tbl_event");
$maxid=mysql_result($res1, 0);

$maxid++;


 $ext = end((explode(".", $name))); # extra () to prevent notice
$cover =  $maxid.".".$ext;



  //move_uploaded_file($_FILES['f1']['tmp_name'],'../projectcoverphoto/'.$cover);
//     $ext = end((explode(".", $name))); # extra () to prevent notice
// $filename =  $id.".".$ext;

 $url ="../event/$cover";
 move_uploaded_file($_FILES["f1"]["tmp_name"], $url);

 $res=mysql_query("insert into  tbl_event values('$cover','$t1','$t2','$t3','$t4','$d','0')");
 

 $countfiles = count($_FILES['f2']['name']);


 // Looping all files
 for($i=0;$i<$countfiles;$i++)
 {


 $name1=$_FILES['f2']['name'][$i]   ;
$ext1 = end((explode(".", $name1))); # extra () to prevent notice



  $filename = "$maxid$i.$ext1";

  // Upload file
  move_uploaded_file($_FILES['f2']['tmp_name'][$i],'../eventphoto/'.$filename);

//echo "insert into  tbl_projects values('$cid','$filename','$t1','$t2','$t3','$d','0')";

$res=mysql_query("insert into  tbl_event_photo values('$maxid','$filename','$d','0')");
}

 if(mysql_affected_rows()>0)
{
msg("Event Registration Completed","event.php");
}
else
{
    
   msg("Error","event.php"); 
}
}


?>

                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

                    <div class="row match-height">
                        <div class="col-md-12 col-xs-12">
                            <div class="card">

                              
                                <div class="card-body">
                                     <div class="row">
                  <div class="col-12">
                     <div class="card">
                        <div class="card-body">
                           <h4 class="header-title mt-0">View list</h4>
                           <div class="table-responsive dash-social">
                              <table id="datatable" class="table table-bordered">
                                 <thead>
                                    <tr>
                                        <th>SlNO</th>
                                          <th>Photo</th>
                                       <th>Title</th>
                                       <th>Address</th>
                                         <th>Description</th>
                                       <th>Date & Time</th>
                                       <th>View</th>
                                         
                                    </tr>
                                    <!--end tr-->
                                 </thead>
                                 <tbody>



                                    <?php


                                    if(isset($_POST['b1']))
                                    {
$t1=$_POST['txt1'];
 $res=mysql_query("select * from tbl_event where name like '$t1%' or reboundid  like '$t1%' order by id desc");
                                    }
                                    else

                                    {

                                        $res=mysql_query("select * from tbl_event order by id desc");
                                    }
$i=0;
                                   
                                    while($row=mysql_fetch_row($res))
                                    {$i++;
?>
 <tr>
   <td><?php echo $i; ?></td><td><img src="../event/<?php echo $row[0]; ?>" class="img img-responsive" style="    height: 100px;
    width: 100px;" ></td>
                                       <td><i class="mdi mdi-google-play text-warning mr-1 font-18"></i><?php  echo $row[1];?></td>
                                       <td><?php  echo $row[2];?></td>
                                       <td><?php  echo $row[3];?></td>
                                        <td><?php  echo $row[4];?></td>
                                       <!-- <td><a href="view.php?v=<?php echo $row[4];  ?>" ><i class="fa fa-eye"></a></td>
                                       <td> -->
 <td>
      <a href="#" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#getphotos" title="Edit" data-id="<?php echo $row[11]; ?>" id="getphoto1"><i class="ft-image"></i> </a>
  <a href="#" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#getphotos" title="Edit" data-id="<?php echo $row[11]; ?>" id="getphoto"><i class="ft-edit-2"></i> </a>  <a href="javascript:delete_id(<?php echo $row[11]; ?>)" class="btn btn-primary btn-xs" data-toggle="tooltip" data-original-title="Delete"> <i class="ft-delete"></i> </a>    </td>
                                      
                                    </tr>

<?php

                                    }
                                    ?>
                               
                                    <!--end tr-->
                                 </tbody>
                              </table>
                           </div>
                        </div>
                        <!--end card-body-->
                     </div>
                     <!--end card-->
                  </div>
                  <!--end col-->
               </div>
                                </div>
                                <!-- <div class="card-footer border-top-blue-grey border-top-lighten-5 text-muted">
                                    <!-- <span class="float-left">3 hours ago</span> -->
                                    <!-- <button type="button" data-toggle="modal" data-target="#iconForm" class="btn btn-glow btn-bg-gradient-x-purple-blue col-12 col-md-5 mr-1 mb-1">Edit</button>
                                </div> --> 
                            </div>
                        </div>
           
                    </div>
                </div>
            </div>








            </div>
        </div>
    </div>



   <footer class="footer footer-static footer-light navbar-border navbar-shadow">
        <div class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2"><span class="float-md-left d-block d-md-inline-block">2018  &copy; Copyright Team Rebound . All right Reserved. Developed by <a class="text-bold-800 grey darken-2" href="https://tahcom.com/" target="_blank">Tahcom</a></span>

        </div>
    </footer>


    <!-- Modal -->
   


<div class="modal fade text-left" id="getphotos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel34" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="myModalLabel34">View Details</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
               <div id="dynamic-content2"></div>
            </div>
        </div>
    </div>

<div class="modal fade text-left" id="getphoto1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel34" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="myModalLabel34">View Photos</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
               <div id="dynamic-content3"></div>
            </div>
        </div>
    </div>

    <!-- BEGIN VENDOR JS-->
    <script src="app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/forms/toggle/switchery.min.js" type="text/javascript"></script>
    <script src="app-assets/js/scripts/forms/switch.min.js" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="app-assets/vendors/js/charts/chartist.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/charts/chartist-plugin-tooltip.min.js" type="text/javascript"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN CHAMELEON  JS-->
    <script src="app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="app-assets/js/core/app.min.js" type="text/javascript"></script>
    <script src="app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/jquery.sharrre.js" type="text/javascript"></script>
    <!-- END CHAMELEON  JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="app-assets/js/scripts/pages/dashboard-analytics.min.js" type="text/javascript"></script>
   





   <script src="assets/js/bootstrap.bundle.min.js"></script>
      <script src="assets/js/jquery.slimscroll.min.js"></script>
      <script src="assets/plugins/moment/moment.js"></script>
      <script src="assets/plugins/apexcharts/apexcharts.min.js"></script>



      
      <script src="assets/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js"></script>
      <script src="assets/plugins/jvectormap/jquery-jvectormap-us-aea-en.js"></script><!-- Required datatable js -->


      <script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
      <script src="assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
      <script src="assets/pages/jquery.dashboard-2.init.js"></script><!-- App js -->
    <!-- END PAGE LEVEL JS-->
    <script type="text/javascript">
      

      $(document).ready(function(){
    
    $(document).on('click', '#getphoto', function(e){
        
        e.preventDefault();
        
      var uid = $(this).data('id');   // it will get id of clicked row
      //var uid=1;
    // alert(uid);
        $('#dynamic-content2').html(''); // leave it blank before ajax call
        $('#modal-loader').show();      // load ajax loader
        
        $.ajax({
            url: 'getevent.php',
            type: 'POST',
            data: 'id='+uid,
            dataType: 'html'
        })
        .done(function(data){
            console.log(data);  
            $('#dynamic-content2').html('');    
            $('#dynamic-content2').html(data); // load response 
          //  alert("ok");
            //$('#modal-loader').hide();        // hide ajax loader   
        })
        .fail(function(){
            $('#dynamic-content2').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Ple    ase try again...');
            $('#modal-loader').hide();
        });
        
    });
    
});


   $(document).ready(function(){
    
    $(document).on('click', '#getphoto1', function(e){
        
        e.preventDefault();
        
      var uid = $(this).data('id');   // it will get id of clicked row
      //var uid=1;
    // alert(uid);
        $('#dynamic-content2').html(''); // leave it blank before ajax call
        $('#modal-loader').show();      // load ajax loader
        
        $.ajax({
            url: 'geteventphoto.php',
            type: 'POST',
            data: 'id='+uid,
            dataType: 'html'
        })
        .done(function(data){
            console.log(data);  
            $('#dynamic-content2').html('');    
            $('#dynamic-content2').html(data); // load response 
          //  alert("ok");
            //$('#modal-loader').hide();        // hide ajax loader   
        })
        .fail(function(){
            $('#dynamic-content2').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Ple    ase try again...');
            $('#modal-loader').hide();
        });
        
    });
    
});
</script>
 
</body>

</html>