-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 15, 2019 at 07:53 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_team`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_club`
--

CREATE TABLE IF NOT EXISTS `tbl_club` (
  `club` varchar(500) NOT NULL,
  `fromdt` varchar(500) NOT NULL,
  `des` varchar(5000) NOT NULL,
  `reboundid` varchar(500) NOT NULL,
  `pdate` varchar(32) NOT NULL,
  `id` int(50) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `tbl_club`
--

INSERT INTO `tbl_club` (`club`, `fromdt`, `des`, `reboundid`, `pdate`, `id`) VALUES
('BBC Notingham', '2002 - 2004', 'Fussy penguin insect additionally wow absolutely crud meretriciously hastily dalmatian a glowered. outside oh arrogantly vehement.', 'TR0013', '13/06/19', 11),
('BBC Notingham', '2012 - 2015', 'That one rank beheld bluebird after outside ignobly allegedly more when oh arrogantly vehement irresistibly fussy penguin insect additionally.', 'TR0013', '13/06/19', 12),
('BBC Notingham ', '2002 - 2004', 'Fussy penguin insect additionally wow absolutely crud meretriciously hastily dalmatian a glowered. outside oh arrogantly vehement.', 'TR0013', '13/06/19', 13),
('cvb df ', 'vbn dsf', 'cvb vb ds', 'TR0014', '13/06/19', 14),
('fgh', '2014 - 2015', 'hjk', 'TR0015', '14/06/19', 15),
('fgh', '2014 - 2015', 'fgh', 'TR0015', '14/06/19', 16);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_eventreg`
--

CREATE TABLE IF NOT EXISTS `tbl_eventreg` (
  `name` varchar(500) NOT NULL,
  `v` varchar(500) NOT NULL,
  `email` varchar(500) NOT NULL,
  `phone` int(32) NOT NULL,
  `no` varchar(5) NOT NULL,
  `gender` varchar(12) NOT NULL,
  `dob` varchar(32) NOT NULL,
  `spouse` varchar(5) NOT NULL,
  `acc` varchar(5) NOT NULL,
  `play` varchar(5) NOT NULL,
  `address` varchar(5000) NOT NULL,
  `s` varchar(500) NOT NULL,
  `m` varchar(5) NOT NULL,
  `l` varchar(5) NOT NULL,
  `xl` varchar(5) NOT NULL,
  `xxl` varchar(5) NOT NULL,
  `xxxl` varchar(5) NOT NULL,
  `m2` varchar(5) NOT NULL,
  `pdate` varchar(32) NOT NULL,
  `id` int(5) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_eventreg`
--

INSERT INTO `tbl_eventreg` (`name`, `v`, `email`, `phone`, `no`, `gender`, `dob`, `spouse`, `acc`, `play`, `address`, `s`, `m`, `l`, `xl`, `xxl`, `xxxl`, `m2`, `pdate`, `id`) VALUES
('test', 'test', 'soumyaavani11@gmail.com', 543, '3', 'Male', '15/06/2019', 'Yes', 'Yes', 'Yes', 'dfgdf', '1', '', '', '3', '', '', '5', '15/06/19', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_log`
--

CREATE TABLE IF NOT EXISTS `tbl_log` (
  `username` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_log`
--

INSERT INTO `tbl_log` (`username`, `password`, `id`) VALUES
('21232f297a57a5a743894a0e4a801fc3', '1064643d7f0b49f1fe408d9a44347476', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_reg`
--

CREATE TABLE IF NOT EXISTS `tbl_reg` (
  `name` varchar(500) NOT NULL,
  `location` varchar(500) NOT NULL,
  `email` varchar(500) NOT NULL,
  `phone` int(32) NOT NULL,
  `username` varchar(500) NOT NULL,
  `password` varchar(500) NOT NULL,
  `dob` varchar(32) NOT NULL,
  `gender` varchar(12) NOT NULL,
  `photo` varchar(50) NOT NULL,
  `address` varchar(5000) NOT NULL,
  `about` varchar(5000) NOT NULL,
  `reboundid` varchar(500) NOT NULL,
  `pdate` varchar(32) NOT NULL,
  `id` int(5) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `tbl_reg`
--

INSERT INTO `tbl_reg` (`name`, `location`, `email`, `phone`, `username`, `password`, `dob`, `gender`, `photo`, `address`, `about`, `reboundid`, `pdate`, `id`) VALUES
('soumya', 'trivandrum', 'spradeep@goldenetqan.com', 123456, 'soumya', 'e10adc3949ba59abbe56e057f20f883e', '13/06/2019', 'on', 'TR001.jpg', 'vbnv', 'vbn', 'TR001', '13/06/19', 12),
('Christino Fischer M', 'Netherlands', 'soumyaavani11@gmail.com', 2147483647, 'tahcom', 'e10adc3949ba59abbe56e057f20f883e', '13/06/2019', 'Male', 'TR0013.jpg', 'Netherlands, Rotterdam', 'Hello my name is Christino Fischer and Iï¿½m a Financial Supervisor from Netherlands, Rotterdam. In pharetra orci dignissim, blandit mi semper, ultricies diam. Suspendisse malesuada suscipit nunc non volutpat. Sed porta nulla id orci laoreet tempor non consequat enim. Sed vitae aliquam velit. Aliquam ante accumsan ac est.', 'TR0013', '14/06/19', 13);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_social`
--

CREATE TABLE IF NOT EXISTS `tbl_social` (
  `reboundid` varchar(500) NOT NULL,
  `fb` varchar(5000) NOT NULL,
  `tw` varchar(5000) NOT NULL,
  `ins` varchar(5000) NOT NULL,
  `id` int(5) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_social`
--

INSERT INTO `tbl_social` (`reboundid`, `fb`, `tw`, `ins`, `id`) VALUES
('TR0013', 'www.facebook.com', 'www.twitter.com', 'www.instagram.com', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
