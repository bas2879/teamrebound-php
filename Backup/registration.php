<!doctype html>
<?php include("connection.php"); ?>
<html class=no-js lang="">
   <!-- Mirrored from 0effortthemes.com/soccerclubv2/blog.php by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 28 May 2019 11:59:29 GMT -->
   <!-- Added by HTTrack -->
   <meta http-equiv="content-type" content="text/html;charset=utf-8" />
   <!-- /Added by HTTrack -->
   <head>
      <meta charset=utf-8>
      <meta name=description content="">
      <meta name=viewport content="width=device-width, initial-scale=1">
      <title>Team Rebound</title>
      <link rel="shortcut icon" href=favicon.ico>
      <link rel=stylesheet href=vendor.css>
      <link rel=stylesheet href=style.css>
      <link rel=stylesheet type=text/css href=css/layerslider.css>
      <link rel="stylesheet" href="css/jquery.dateselect.css">
       <link rel="stylesheet" href="css/newtable.css">
      <script src=js/vendor/modernizr.js></script>

       <link rel="stylesheet" href="css/elementadd.css">
   </head>
   <body>
      <!--[if lt IE 10]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
      <![endif]-->
      <div class=wrapper>
         <header class=header-main>
            <div class="header-lower clearfix">
               <div class=container>
                  <div class=row>
                     <h1 class=logo><a href=index.php><img src=images/logo.png alt=image></a></h1>
                     <div class=menubar>
                        <nav class=navbar>
                           <div class=nav-wrapper>
                              <div class=navbar-header><button type=button class=navbar-toggle><span class=sr-only>Toggle navigation</span> <span class=icon-bar></span></button></div>
                              <div class=nav-menu>
                                 <ul class="nav navbar-nav menu-bar">
                                    <li><a href=index.php >Home</a></li>
                                    <li><a href=about.php>about Us</a></li>
                                    <li><a href=gallery.php>Gallery</a></li>
                                    <li><a href=news.php>News & Events</a></li>
                                    <li><a href=contact.php>Contact Us</a></li>
                                    <li><a href=signup.php >Registration</a></li>
                                    <li><a href=login.php class=active>SignIn</a></li>
                                    
                                    
                                 </ul>
                              </div>
                           </div>
                        </nav>
                     </div>
                     <div class=social>
                      <a href=https://www.facebook.com/ class=facebook><i class="fa fa-facebook"></i></a> 
                      <a href=https://twitter.com/ class=twitter><i class="fa fa-twitter"></i></a> 
                     </div>
                  </div>
               </div>
            </div>
         </header>
         <div class=innerbannerwrap>
            <div class=content></div>
            <div class=innerbanner5>
               <h2 class=bannerHeadline>Rebound <span> Registration</span></h2>
            </div>
         </div>
        
         <section class="innerpage_all_wrap bg-white">
            <div class=container>
               <div class=row>
                  <h2 class=heading>Register through<span> Team Rebound</span></h2>
                  <p class=headParagraph>On registration, users define themselves as professionals.</p>
                  <div class=innerWrapper>
                     <main class=contentinner>
                        <div class=blogDetails>
                          
                           <div class="blog_info">

            <form data-parsley-validate="" name=contact class="formcontact fc   clearfix" method="post">
                           <div class=form-group>
                          <input type=text class=form-control name=name placeholder=Fullname required="" data-parsley-required-message="please insert Name">
                        </div>
                         <div class=form-group>
                          <input type=text class=form-control name=loc placeholder=Location required="" data-parsley-required-message="please insert Location">
                        </div>
                        <div class=form-group>
                          <input type=email class=form-control name=email placeholder=Email required="" data-parsley-required-message="please insert Email">
                        </div>
                                           
                        <div class=form-group>
                          <input type=text class=form-control name=phone placeholder=Phone required="" data-parsley-required-message="please insert Phone No">
                        </div>



                        <div class=form-group>
                          <input type=text class=form-control name=subject placeholder="Number of persons attending (Above 10 yrs including self)"
 required="" data-parsley-required-message="please insert number">
                        </div>

                        <div class=form-group>
                <div class="form-check">
                  <input type="radio" style="height: 12px !important;" id="materialChecked" name="GENDER" checked value="Male">
                  <label class="" for="materialUnchecked">Male</label>

                  <input type="radio" style="height: 12px !important;" id="materialChecked"  name="GENDER" value="Female">
                  <label class="" for="materialChecked">Female</label>

                  <input type="radio" style="height: 12px !important;" id="materialChecked" name="GENDER" value="Others"  >
                  <label class="" for="materialChecked">Others</label>
                </div>
              </div>



                        <div class=form-group>
                          <input type="text" name="date1" id="date1" class="form-control" data-select="date" placeholder="DOB">
                        </div>



                        <div class=form-group>
                <div class="form-check">
                  <p>Attending with spouse ?</p>
                  <input type="radio" style="height: 12px !important;" id="materialChecked" name="spouse" checked value="Yes">
                  <label class="" for="materialUnchecked">Yes</label>
                  <input type="radio" style="height: 12px !important;" id="materialChecked"  name="spouse" value="No" >
                  <label class="" for="materialChecked">No</label>
                </div>
              </div>

              <div class=form-group>
                <div class="form-check">
                  <p>Accommodation Required ?</p>
                  <input type="radio" style="height: 12px !important;" id="materialChecked" name="acc" checked value="Yes">
                  <label class="" for="materialUnchecked">Yes</label>
                  <input type="radio" style="height: 12px !important;" id="materialChecked"  name="acc" value="No" >
                  <label class="" for="materialChecked">No</label>
                </div>
              </div>
              <div class=form-group>
                <div class="form-check">
                  <p>Are you playing ?</p>
                  <input type="radio" style="height: 12px !important;" id="materialChecked" name="play" checked  value="Yes">
                  <label class="" for="materialUnchecked">Yes</label>
                  <input type="radio" style="height: 12px !important;" id="materialChecked"  name="play" value="No" >
                  <label class="" for="materialChecked">No</label>
                </div>
              </div>

              <div class=form-group>
                          <input type=text class=form-control name=address placeholder=Address required="" data-parsley-required-message="please insert your address">
                        </div>


                        <div class=form-group1>
                          
                          <div role="listitem" class="freebirdFormviewerViewItemsItemItem" jsname="ibnC6b" jscontroller="BslRAd" jsaction="rcuQ6b:WYd;sPvj8e:XyQaue;JIbuQc:Ptdedd(RKTzrd)" data-item-id="1050127262">
   <div class="freebirdFormviewerViewItemsItemItemHeader">
      <div >T-Shirt Size</div>
   </div>
   <div class="freebirdFormviewerViewItemsGridContainer" jsname="Soz5ie" aria-label="T-Shirt Size">
      <div class="freebirdFormviewerViewItemsGridInnerContainer">
         <div class="freebirdFormviewerViewItemsGridScrollContainer" jsname="iyUusd">
            <div class="freebirdFormviewerViewItemsGridScrollingData" jsname="USX4tf">
               <div class="freebirdFormviewerViewItemsGridRow freebirdFormviewerViewItemsGridColumnHeader">
                  <div class="freebirdFormviewerViewItemsGridCell freebirdFormviewerViewItemsGridRowHeader"></div>
                  <div class="freebirdFormviewerViewItemsGridCell">1</div>
                  <div class="freebirdFormviewerViewItemsGridCell">2</div>
                  <div class="freebirdFormviewerViewItemsGridCell">3</div>
                  <div class="freebirdFormviewerViewItemsGridCell">4</div>
                  <div class="freebirdFormviewerViewItemsGridCell">5</div>
                  <div class="freebirdFormviewerViewItemsGridCell">6</div>
                  <div class="freebirdFormviewerViewItemsGridCell">0</div>
               </div>





               <div class="freebirdFormviewerViewItemsGridUngraded freebirdFormviewerViewItemsGridRowGroup" jscontroller="wPRNsd" jsshadow="" jsaction="keydown: I481le;JIbuQc:JIbuQc;rcuQ6b:rcuQ6b" jsname="NYpehe" data-row-index="0" aria-describedby="i.desc.1050127262 i.err.1050127262" aria-label="S" role="radiogroup">
                  <span jsslot="" role="presentation" jsname="bN97Pc" class="exportContent">
                     <div class="freebirdFormviewerViewItemsGridCell freebirdFormviewerViewItemsGridRowHeader">S</div>
                     <div class="freebirdFormviewerViewItemsGridCell">
                        <div class="quantumWizTogglePaperradioRadioContainer">
                            <div><input type="radio" style="height: 20px !important;" id="size"  name="size" value="1" ></div>
                           </div>
                     </div>

                     <div class="freebirdFormviewerViewItemsGridCell">
                        <div class="quantumWizTogglePaperradioRadioContainer">
                            <div><input type="radio" style="height: 20px !important;" id="size"  name="size"  value="2" ></div>
                           </div>
                     </div>

                      <div class="freebirdFormviewerViewItemsGridCell">
                        <div class="quantumWizTogglePaperradioRadioContainer">
                            <div><input type="radio" style="height: 20px !important;" id="size"  name="size"  value="3"></div>
                           </div>
                     </div>

                      <div class="freebirdFormviewerViewItemsGridCell">
                        <div class="quantumWizTogglePaperradioRadioContainer">
                            <div><input type="radio" style="height: 20px !important;" id="size"  name="size"  value="4" ></div>
                           </div>
                     </div>

                      <div class="freebirdFormviewerViewItemsGridCell">
                        <div class="quantumWizTogglePaperradioRadioContainer">
                            <div><input type="radio" style="height: 20px !important;" id="size"  name="size"  value="5" ></div>
                           </div>
                     </div>

                      <div class="freebirdFormviewerViewItemsGridCell">
                        <div class="quantumWizTogglePaperradioRadioContainer">
                            <div><input type="radio" style="height: 20px !important;" id="size"  name="size"  value="6" ></div>
                           </div>
                     </div>
                    
                      <div class="freebirdFormviewerViewItemsGridCell">
                        <div class="quantumWizTogglePaperradioRadioContainer">
                            <div><input type="radio" style="height: 20px !important;" id="size"  name="size"  value="0"></div>
                           </div>
                     </div>

                  </span>
               </div>

              
               <div class="freebirdFormviewerViewItemsGridUngraded freebirdFormviewerViewItemsGridRowGroup" jscontroller="wPRNsd" jsshadow="" jsaction="keydown: I481le;JIbuQc:JIbuQc;rcuQ6b:rcuQ6b" jsname="NYpehe" data-row-index="0" aria-describedby="i.desc.1050127262 i.err.1050127262" aria-label="M" role="radiogroup">
                  <span jsslot="" role="presentation" jsname="bN97Pc" class="exportContent">
                     <div class="freebirdFormviewerViewItemsGridCell freebirdFormviewerViewItemsGridRowHeader">M</div>
                     <div class="freebirdFormviewerViewItemsGridCell">
                        <div class="quantumWizTogglePaperradioRadioContainer">
                            <div><input type="radio" style="height: 20px !important;" id="sizem"  name="sizem"  value="1" ></div>
                           </div>
                     </div>

                     <div class="freebirdFormviewerViewItemsGridCell">
                        <div class="quantumWizTogglePaperradioRadioContainer">
                            <div><input type="radio" style="height: 20px !important;" id="sizem"  name="sizem" value="2" ></div>
                           </div>
                     </div>

                      <div class="freebirdFormviewerViewItemsGridCell">
                        <div class="quantumWizTogglePaperradioRadioContainer">
                            <div><input type="radio" style="height: 20px !important;" id="sizem"  name="sizem" value="3" ></div>
                           </div>
                     </div>

                      <div class="freebirdFormviewerViewItemsGridCell">
                        <div class="quantumWizTogglePaperradioRadioContainer">
                            <div><input type="radio" style="height: 20px !important;" id="sizem"  name="sizem" value="4" ></div>
                           </div>
                     </div>

                      <div class="freebirdFormviewerViewItemsGridCell">
                        <div class="quantumWizTogglePaperradioRadioContainer">
                            <div><input type="radio" style="height: 20px !important;" id="sizem"  name="sizem" value="5" ></div>
                           </div>
                     </div>

                      <div class="freebirdFormviewerViewItemsGridCell">
                        <div class="quantumWizTogglePaperradioRadioContainer">
                            <div><input type="radio" style="height: 20px !important;" id="sizem"  name="sizem" value="6"></div>
                           </div>
                     </div>
                    
                      <div class="freebirdFormviewerViewItemsGridCell">
                        <div class="quantumWizTogglePaperradioRadioContainer">
                            <div><input type="radio" style="height: 20px !important;" id="sizem"  name="sizem" value="0" ></div>
                           </div>
                     </div>

                  </span>
               </div>

               <div class="freebirdFormviewerViewItemsGridUngraded freebirdFormviewerViewItemsGridRowGroup" jscontroller="wPRNsd" jsshadow="" jsaction="keydown: I481le;JIbuQc:JIbuQc;rcuQ6b:rcuQ6b" jsname="NYpehe" data-row-index="0" aria-describedby="i.desc.1050127262 i.err.1050127262" aria-label="L" role="radiogroup">
                  <span jsslot="" role="presentation" jsname="bN97Pc" class="exportContent">
                     <div class="freebirdFormviewerViewItemsGridCell freebirdFormviewerViewItemsGridRowHeader">L</div>
                     <div class="freebirdFormviewerViewItemsGridCell">
                        <div class="quantumWizTogglePaperradioRadioContainer">
                            <div><input type="radio" style="height: 20px !important;" id="sizeL"  name="sizeL" value="1" ></div>
                           </div>
                     </div>

                     <div class="freebirdFormviewerViewItemsGridCell">
                        <div class="quantumWizTogglePaperradioRadioContainer">
                            <div><input type="radio" style="height: 20px !important;" id="sizeL"  name="sizeL" value="2"></div>
                           </div>
                     </div>

                      <div class="freebirdFormviewerViewItemsGridCell">
                        <div class="quantumWizTogglePaperradioRadioContainer">
                            <div><input type="radio" style="height: 20px !important;" id="sizeL"  name="sizeL" value="3"></div>
                           </div>
                     </div>

                      <div class="freebirdFormviewerViewItemsGridCell">
                        <div class="quantumWizTogglePaperradioRadioContainer">
                            <div><input type="radio" style="height: 20px !important;" id="sizeL"  name="sizeL" value="4"></div>
                           </div>
                     </div>

                      <div class="freebirdFormviewerViewItemsGridCell">
                        <div class="quantumWizTogglePaperradioRadioContainer">
                            <div><input type="radio" style="height: 20px !important;" id="sizeL"  name="sizeL" value="5"></div>
                           </div>
                     </div>

                      <div class="freebirdFormviewerViewItemsGridCell">
                        <div class="quantumWizTogglePaperradioRadioContainer">
                            <div><input type="radio" style="height: 20px !important;" id="sizeL"  name="sizeL" value="6"></div>
                           </div>
                     </div>
                    
                      <div class="freebirdFormviewerViewItemsGridCell">
                        <div class="quantumWizTogglePaperradioRadioContainer">
                            <div><input type="radio" style="height: 20px !important;" id="sizeL"  name="sizeL" value="0"></div>
                           </div>
                     </div>

                  </span>
               </div>
              
               <div class="freebirdFormviewerViewItemsGridUngraded freebirdFormviewerViewItemsGridRowGroup" jscontroller="wPRNsd" jsshadow="" jsaction="keydown: I481le;JIbuQc:JIbuQc;rcuQ6b:rcuQ6b" jsname="NYpehe" data-row-index="0" aria-describedby="i.desc.1050127262 i.err.1050127262" aria-label="XL" role="radiogroup">
                  <span jsslot="" role="presentation" jsname="bN97Pc" class="exportContent">
                     <div class="freebirdFormviewerViewItemsGridCell freebirdFormviewerViewItemsGridRowHeader">XL</div>
                     <div class="freebirdFormviewerViewItemsGridCell">
                        <div class="quantumWizTogglePaperradioRadioContainer">
                            <div><input type="radio" style="height: 20px !important;" id="sizeXL"  name="sizeXL" value="1"></div>
                           </div>
                     </div>

                     <div class="freebirdFormviewerViewItemsGridCell">
                        <div class="quantumWizTogglePaperradioRadioContainer">
                            <div><input type="radio" style="height: 20px !important;" id="sizeXL"  name="sizeXL" value="2"></div>
                           </div>
                     </div>

                      <div class="freebirdFormviewerViewItemsGridCell">
                        <div class="quantumWizTogglePaperradioRadioContainer">
                            <div><input type="radio" style="height: 20px !important;" id="sizeXL"  name="sizeXL" value="3"></div>
                           </div>
                     </div>

                      <div class="freebirdFormviewerViewItemsGridCell">
                        <div class="quantumWizTogglePaperradioRadioContainer">
                            <div><input type="radio" style="height: 20px !important;" id="sizeXL"  name="sizeXL" value="4"></div>
                           </div>
                     </div>

                      <div class="freebirdFormviewerViewItemsGridCell">
                        <div class="quantumWizTogglePaperradioRadioContainer">
                            <div><input type="radio" style="height: 20px !important;" id="sizeXL"  name="sizeXL" value="5" ></div>
                           </div>
                     </div>

                      <div class="freebirdFormviewerViewItemsGridCell">
                        <div class="quantumWizTogglePaperradioRadioContainer">
                            <div><input type="radio" style="height: 20px !important;" id="sizeXL"  name="sizeXL" value="6"></div>
                           </div>
                     </div>
                    
                      <div class="freebirdFormviewerViewItemsGridCell">
                        <div class="quantumWizTogglePaperradioRadioContainer">
                            <div><input type="radio" style="height: 20px !important;" id="sizeXL"  name="sizeXL" value="0"></div>
                           </div>
                     </div>

                  </span>
               </div>

               <div class="freebirdFormviewerViewItemsGridUngraded freebirdFormviewerViewItemsGridRowGroup" jscontroller="wPRNsd" jsshadow="" jsaction="keydown: I481le;JIbuQc:JIbuQc;rcuQ6b:rcuQ6b" jsname="NYpehe" data-row-index="0" aria-describedby="i.desc.1050127262 i.err.1050127262" aria-label="XXL" role="radiogroup">
                  <span jsslot="" role="presentation" jsname="bN97Pc" class="exportContent">
                     <div class="freebirdFormviewerViewItemsGridCell freebirdFormviewerViewItemsGridRowHeader">XXL</div>
                     <div class="freebirdFormviewerViewItemsGridCell">
                        <div class="quantumWizTogglePaperradioRadioContainer">
                            <div><input type="radio" style="height: 20px !important;" id="sizeXXL"  name="sizeXXL" value="1" ></div>
                           </div>
                     </div>

                     <div class="freebirdFormviewerViewItemsGridCell">
                        <div class="quantumWizTogglePaperradioRadioContainer">
                            <div><input type="radio" style="height: 20px !important;" id="sizeXXL"  name="sizeXXL" value="2" ></div>
                           </div>
                     </div>

                      <div class="freebirdFormviewerViewItemsGridCell">
                        <div class="quantumWizTogglePaperradioRadioContainer">
                            <div><input type="radio" style="height: 20px !important;" id="sizeXXL"  name="sizeXXL" value="3"></div>
                           </div>
                     </div>

                      <div class="freebirdFormviewerViewItemsGridCell">
                        <div class="quantumWizTogglePaperradioRadioContainer">
                            <div><input type="radio" style="height: 20px !important;" id="sizeXXL"  name="sizeXXL" value="4" ></div>
                           </div>
                     </div>

                      <div class="freebirdFormviewerViewItemsGridCell">
                        <div class="quantumWizTogglePaperradioRadioContainer">
                            <div><input type="radio" style="height: 20px !important;" id="sizeXXL"  name="sizeXXL" value="5" ></div>
                           </div>
                     </div>

                      <div class="freebirdFormviewerViewItemsGridCell">
                        <div class="quantumWizTogglePaperradioRadioContainer">
                            <div><input type="radio" style="height: 20px !important;" id="sizeXXL"  name="sizeXXL" value="6" ></div>
                           </div>
                     </div>
                    
                      <div class="freebirdFormviewerViewItemsGridCell">
                        <div class="quantumWizTogglePaperradioRadioContainer">
                            <div><input type="radio" style="height: 20px !important;" id="sizeXXL"  name="sizeXXL" value="0" ></div>
                           </div>
                     </div>

                  </span>
               </div>
              
               <div class="freebirdFormviewerViewItemsGridUngraded freebirdFormviewerViewItemsGridRowGroup" jscontroller="wPRNsd" jsshadow="" jsaction="keydown: I481le;JIbuQc:JIbuQc;rcuQ6b:rcuQ6b" jsname="NYpehe" data-row-index="0" aria-describedby="i.desc.1050127262 i.err.1050127262" aria-label="XXXL" role="radiogroup">
                  <span jsslot="" role="presentation" jsname="bN97Pc" class="exportContent">
                     <div class="freebirdFormviewerViewItemsGridCell freebirdFormviewerViewItemsGridRowHeader">XXXL</div>
                     <div class="freebirdFormviewerViewItemsGridCell">
                        <div class="quantumWizTogglePaperradioRadioContainer">
                            <div><input type="radio" style="height: 20px !important;" id="sizeXXXL"  name="sizeXXXL"  value="1"></div>
                           </div>
                     </div>

                     <div class="freebirdFormviewerViewItemsGridCell">
                        <div class="quantumWizTogglePaperradioRadioContainer">
                            <div><input type="radio" style="height: 20px !important;" id="sizeXXXL"  name="sizeXXXL" value="2" ></div>
                           </div>
                     </div>

                      <div class="freebirdFormviewerViewItemsGridCell">
                        <div class="quantumWizTogglePaperradioRadioContainer">
                            <div><input type="radio" style="height: 20px !important;" id="sizeXXXL"  name="sizeXXXL" value="3" ></div>
                           </div>
                     </div>

                      <div class="freebirdFormviewerViewItemsGridCell">
                        <div class="quantumWizTogglePaperradioRadioContainer">
                            <div><input type="radio" style="height: 20px !important;" id="sizeXXXL"  name="sizeXXXL" value="4"></div>
                           </div>
                     </div>

                      <div class="freebirdFormviewerViewItemsGridCell">
                        <div class="quantumWizTogglePaperradioRadioContainer">
                            <div><input type="radio" style="height: 20px !important;" id="sizeXXXL"  name="sizeXXXL" value="5"></div>
                           </div>
                     </div>

                      <div class="freebirdFormviewerViewItemsGridCell">
                        <div class="quantumWizTogglePaperradioRadioContainer">
                            <div><input type="radio" style="height: 20px !important;" id="sizeXXXL"  name="sizeXXXL" value="6"></div>
                           </div>
                     </div>
                    
                      <div class="freebirdFormviewerViewItemsGridCell">
                        <div class="quantumWizTogglePaperradioRadioContainer">
                            <div><input type="radio" style="height: 20px !important;" id="sizeXXXL"  name="sizeXXXL" value="0"></div>
                           </div>
                     </div>

                  </span>
               </div>

               <div class="freebirdFormviewerViewItemsGridUngraded freebirdFormviewerViewItemsGridRowGroup" jscontroller="wPRNsd" jsshadow="" jsaction="keydown: I481le;JIbuQc:JIbuQc;rcuQ6b:rcuQ6b" jsname="NYpehe" data-row-index="0" aria-describedby="i.desc.1050127262 i.err.1050127262" aria-label="M" role="radiogroup">
                  <span jsslot="" role="presentation" jsname="bN97Pc" class="exportContent">
                     <div class="freebirdFormviewerViewItemsGridCell freebirdFormviewerViewItemsGridRowHeader">M2</div>
                     <div class="freebirdFormviewerViewItemsGridCell">
                        <div class="quantumWizTogglePaperradioRadioContainer">
                            <div><input type="radio" style="height: 20px !important;" id="sizem2"  name="sizem2" value="1" ></div>
                           </div>
                     </div>

                     <div class="freebirdFormviewerViewItemsGridCell">
                        <div class="quantumWizTogglePaperradioRadioContainer">
                            <div><input type="radio" style="height: 20px !important;" id="sizem2"  name="sizem2" value="2" ></div>
                           </div>
                     </div>

                      <div class="freebirdFormviewerViewItemsGridCell">
                        <div class="quantumWizTogglePaperradioRadioContainer">
                            <div><input type="radio" style="height: 20px !important;" id="sizem2"  name="sizem2" value="3" ></div>
                           </div>
                     </div>

                      <div class="freebirdFormviewerViewItemsGridCell">
                        <div class="quantumWizTogglePaperradioRadioContainer">
                            <div><input type="radio" style="height: 20px !important;" id="sizem2"  name="sizem2" value="4"></div>
                           </div>
                     </div>

                      <div class="freebirdFormviewerViewItemsGridCell">
                        <div class="quantumWizTogglePaperradioRadioContainer">
                            <div><input type="radio" style="height: 20px !important;" id="sizem2"  name="sizem2" value="5" ></div>
                           </div>
                     </div>

                      <div class="freebirdFormviewerViewItemsGridCell">
                        <div class="quantumWizTogglePaperradioRadioContainer">
                            <div><input type="radio" style="height: 20px !important;" id="sizem2"  name="sizem2" value="6" ></div>
                           </div>
                     </div>
                    
                      <div class="freebirdFormviewerViewItemsGridCell">
                        <div class="quantumWizTogglePaperradioRadioContainer">
                            <div><input type="radio" style="height: 20px !important;" id="sizem2"  name="sizem2" value="0"></div>
                           </div>
                     </div>

                  </span>
               </div>



            </div>
         </div>
        
         
      </div>
   </div>

</div>





                        </div>
      

                          <div class="form-group1" style="text-align: right;">
 <input type="submit" name="send" id="send"  class="btn btn-red marb" value="Submit" >
  
</div>
                          <!--  <button type=submit class="btn btn-red marb" id=send>Login</button> -->
                         
                                  <?php
if(isset($_POST['send']))
{

      $name=$_POST['name'];
        $loc=$_POST['loc'];
  $email=$_POST['email'];
  $phone=$_POST['phone'];
  $no=$_POST['subject'];
  $gender=$_POST['GENDER'];
  $dob=$_POST['date1'];
$spouse=$_POST['spouse'];
$acc=$_POST['acc'];
$play=$_POST['play'];
  $address=$_POST['address'];


  if(isset($_POST['size']))
  {
  $s=$_POST['size'];
}
else
{

  $s="";
}

 if(isset($_POST['sizem']))
  {
 $m=$_POST['sizem'];
 }
else
{

  $m="";
}

 if(isset($_POST['sizeL']))
  {
 $l=$_POST['sizeL'];
 }
else
{

  $l="";
}
 if(isset($_POST['sizeXL']))
  {
 $xl=$_POST['sizeXL'];
 }
else
{

  $xl="";
}
 if(isset($_POST['sizeXXL']))
  {
 $xxl=$_POST['sizeXXL'];
 }
else
{

  $xxl="";
}

   if(isset($_POST['sizeXXXL']))
  {
 $xxxl=$_POST['sizeXXXL'];
 }
else
{

  $xxxl="";
}

   if(isset($_POST['sizem2']))
  {
 $m2=$_POST['sizem2'];
 }
else
{

  $m2="";
}
 










$d=date("d/m/y");
//echo "insert into tbl_reg values('$name','$loc','$email','$phone','$user','$pass','$dob','$gender','$photo','$address','$about','$rebondid','$d','0')";
mysql_query("insert into tbl_eventreg values('$name','$loc','$email','$phone','$no','$gender','$dob','$spouse','$acc','$play','$address','$s','$m','$l','$xl','$xxl','$xxxl','$m2','$d','0')");

msg("Thank you for registering with us","registration.php");
  //msg("Thank you for registering with us.We will contact you as soon as we review your details.","registration.php");
}


                ?>     
                     </form>

                              
                           </div>
                        </div>
                       
                        
                     </main>
                     <aside class="widgetinner clearfix">
                        <div class=row>
                           
                           
                           <div class=blog_widget>
                              <h4 class=oswald16>follow us</h4>
                              <div class=blog_social><a href=# class=social_link><i class="fa fa-facebook"></i></a> <a href=# class=social_link><i class="fa fa-twitter"></i></a> </div>
                           </div>
                               <div class=blog_widget>
                              <h4 class=oswald16>popular testimonials</h4>
                              <p class=red>@Lorem ipsum dolor sit amet</p>
                              <ul class=widget_commentDetails>
                                 <li>
                                    <a href=# class=clearfix>
                                       <div class=comment-pic>
                                          <div class=columnpic><img src=images/widget/comment01.jpg alt=image></div>
                                       </div>
                                       <div class=commentinfo>
                                          <p>Lorem ipsum dolor sit amet, conse ctetur .</p>
                                          <p class=red>@lorem ipsum</p>
                                       </div>
                                    </a>
                                 </li>
                                 <li>
                                    <a href=# class=clearfix>
                                       <div class=comment-pic>
                                          <div class=columnpic><img src=images/widget/comment02.jpg alt=image></div>
                                       </div>
                                       <div class=commentinfo>
                                          <p>Lorem ipsum dolor sit amet, conse ctetur .</p>
                                          <p class=red>@lorem ipsum</p>
                                       </div>
                                    </a>
                                 </li>
                                 <li>
                                    <a href=# class=clearfix>
                                       <div class=comment-pic>
                                          <div class=columnpic><img src=images/widget/comment03.jpg alt=image></div>
                                       </div>
                                       <div class=commentinfo>
                                          <p>Lorem ipsum dolor sit amet, conse ctetur .</p>
                                          <p class=red>@lorem ipsum</p>
                                       </div>
                                    </a>
                                 </li>
                              </ul>
                           </div>
                          
                           <div class="blog_widget b_twitter">
                              <h4 class=oswald16>Gallery</h4>
                              <ul class="galleryontent04 clearfix">
                                 <li>
                                    <a href=# class=clearfix>
                                       <div style="background:url(images/widget/widgetG01.jpg) no-repeat center"></div>
                                    </a>
                                 </li>
                                 <li>
                                    <a href=# class=clearfix>
                                       <div style="background:url(images/widget/widgetG02.jpg) no-repeat center"></div>
                                    </a>
                                 </li>
                                 <li>
                                    <a href=# class=clearfix>
                                       <div style="background:url(images/widget/widgetG03.jpg) no-repeat center"></div>
                                    </a>
                                 </li>
                                 <li>
                                    <a href=# class=clearfix>
                                       <div style="background:url(images/widget/widgetG04.jpg) no-repeat center"></div>
                                    </a>
                                 </li>
                                 <li>
                                    <a href=# class=clearfix>
                                       <div style="background:url(images/widget/widgetG05.jpg) no-repeat center"></div>
                                    </a>
                                 </li>
                                 <li>
                                    <a href=# class=clearfix>
                                       <div style="background:url(images/widget/widgetG06.jpg) no-repeat center"></div>
                                    </a>
                                 </li>
                                 <li>
                                    <a href=# class=clearfix>
                                       <div style="background:url(images/widget/widgetG07.jpg) no-repeat center"></div>
                                    </a>
                                 </li>
                                 <li>
                                    <a href=# class=clearfix>
                                       <div style="background:url(images/widget/widgetG01.jpg) no-repeat center"></div>
                                    </a>
                                 </li>
                                 <li>
                                    <a href=# class=clearfix>
                                       <div style="background:url(images/widget/widgetG08.jpg) no-repeat center"></div>
                                    </a>
                                 </li>
                              </ul>
                           </div>
                          
                         
                          
                        </div>
                     </aside>
                  </div>
               </div>
            </div>
         </section>
          <footer>
            
            <div class=footer-type02>
               <div class=container>
                  <div class=row>
                     <a href=index.php class=footer-logo><img src=images/logo.png alt=image></a>
                     <div class=footer-container>
                        <ul class=clearfix>
                           <li><a href=https://www.facebook.com class=bigsocial-link><i class="fa fa-facebook"></i></a></li>
                           <li><a href=https://twitter.com class=bigsocial-link target=_blank><i class="fa fa-twitter"></i></a></li>
                         
                        </ul>
                        <p>Copyright, 2019 teamrebound.com. Designed by <a href="https://tahcom.com/" class=copyright target="_blank" >Tahcom</a></p>
                     </div>
                     <div class=footer-appstore>
                        <figure><a href=#><img src=images/appstore/apple.png alt=image></a></figure>
                        <figure><a href=#><img src=images/appstore/google.png alt=image></a></figure>
                     </div>
                  </div>
               </div>
            </div>
         </footer>
      </div>
      <div class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Modal body text goes here.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
      <script src=js/vendor/vendor.js></script><script src=js/main.js></script>
      <script type="text/javascript" src="js/jquery.dateselect.js"></script>


      <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
                            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
                            <script src="js/jquery.czMore-1.5.3.2.js"></script>
                            <script type="text/javascript">
                                //One-to-many relationship plugin by Yasir O. Atabani. Copyrights Reserved.
                                $("#czContainer").czMore();
                            </script>

   </body>
   <!-- Mirrored from 0effortthemes.com/soccerclubv2/blog.php by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 28 May 2019 11:59:50 GMT -->
</html>