
<?php include("connection.php"); ?>
<?php
if(!isset($_SESSION['muser']))
{

  header("location: login.php");
}
?>
<!doctype html>
<html class=no-js lang="">
   <!-- Mirrored from 0effortthemes.com/soccerclubv2/player_details.php by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 28 May 2019 12:00:29 GMT -->
   <!-- Added by HTTrack -->
   <meta http-equiv="content-type" content="text/html;charset=utf-8" />
   <!-- /Added by HTTrack -->
   <head>
      <meta charset=utf-8>
      <meta name=description content="">
      <meta name=viewport content="width=device-width, initial-scale=1">
      <title>Team Rebound</title>
      <link rel="shortcut icon" href=favicon.ico>
      <link rel=stylesheet href=vendor.css>
      <link rel=stylesheet href=style.css>
      <link rel=stylesheet type=text/css href=css/layerslider.css>
      <script src=js/vendor/modernizr.js></script>
   </head>
   <body>
      <!--[if lt IE 10]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
      <![endif]-->
      <div class=wrapper>
         <header class=header-main>
            
            <div class="header-lower clearfix">
               <div class=container>
                  <div class=row>
                     <h1 class=logo><a href=index.php><img src=images/logo.png alt=image></a></h1>
                     <div class=menubar>
                        <nav class=navbar>
                           <div class=nav-wrapper>
                              <div class=navbar-header><button type=button class=navbar-toggle><span class=sr-only>Toggle navigation</span> <span class=icon-bar></span></button></div>
                              <div class=nav-menu>
                                 <ul class="nav navbar-nav menu-bar">
                                    <li><a href=index.php>Home</a></li>
                                    <li><a href=directory.php class="active">Directory</a></li>
                                    <li><a href=#>Logout</a></li>
                                 </ul>
                              </div>
                           </div>
                        </nav>
                     </div>
                     <div class=social><a href=https://www.facebook.com/ class=facebook><i class="fa fa-facebook"></i></a> <a href=https://twitter.com/ class=twitter><i class="fa fa-twitter"></i></a></div>
                  </div>
               </div>
            </div>
         </header>
         <div class=innerbannerwrap>
            <div class=content></div>
            <div class=innerbanner3>
               <h2 class=bannerHeadline>Athlet <span>details</span></h2>
            </div>
         </div>
         <section class=playerDpage>
            <div class=container>
               <div class=row>
                   <div class="bg-red headline01 blog-detailsfooter clearfix">

                                 <div class="blog-detailsfooter02a">
                                    <a href="directory.php" class="btn-small01a clrnew">back to list</a>
                                 </div>
                                 <div>
                                    
                                 </div>
                        </div>

<?php
$id=$_REQUEST['id'];
 $res=mysql_query("select * from tbl_reg where id='$id'");
 $row=mysql_fetch_row($res);
                                   ?>



                  <ul class="playerDetailsPage-info clearfix">
                     <li>
                        <h2><?php echo $row[11]; ?></h2>
                        <h3><?php echo $row[0]; ?></h3>
                        <div class=bio_player>
                           <div><span>Location</span><?php echo $row[1]; ?></div>
                           <div><span>DOB</span><?php echo $row[6]; ?></div>
                           <div><span>Gender</span><?php echo $row[7]; ?></div>
                           <div><span>Email</span><?php echo $row[2]; ?></div>
                           <div><span>Phone</span><?php echo $row[3]; ?></div>
                          <!--  <div><span>born</span>20/01/1995</div>
                           <div><span>club</span>Ghost Scorpions</div> -->
                        </div>
                       
                     </li>
                     <li></li>
                     <li>
                        <h5><?php echo $row[9]; ?></h5>
                        <p><?php echo $row[10]; ?></p>
<?php

 $res1=mysql_query("select * from tbl_social where reboundid='$row[11]'");
 $row1=mysql_fetch_row($res1);
                                   ?>

                       <!--  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita, tempore consequuntur. Magni distinctio nihil.</p> -->
                        <div class="playersocial bg-black clearfix">
                           <a href=<?php echo $row1[1]; ?> class=social_link><i class="fa fa-facebook"></i></a> 
                          
                           <a href=<?php echo $row1[2]; ?> class=social_link><i class="fa fa-twitter"></i></a> 
                         
                        </div>
                     </li>
                  </ul>
                  <div class=information_section>
                     <?php

 $res2=mysql_query("select * from tbl_club where reboundid='$row[11]'");
 while($row2=mysql_fetch_row($res2))
 {

                                   ?>
                     <h4><?php echo $row2[0]; ?>(<?php echo $row2[1]; ?>)</h4>
                     <p><?php echo $row2[2]; ?></p>
                     
                    <?php } ?> 
                  </div>
                 
               </div>
            </div>
         </section>
        
         <section class=sponsors>
            <div class=container>
               <div class=row>
                  <h2 class=heading>exclusive <span>sponsors</span></h2>
                  <p class=headParagraph>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis earum reprehenderit quisquam, veritatis, qui libero velit consequatur alias assumenda corporis aut quibusdam, sint numquam voluptatem sapiente doloribus ipsum odit nihil!</p>
                  <ul class="client clearfix">
                     <li><a href="#"><img src=images/client/client01.png alt=image></a></li>
                     <li><a href="#"><img src=images/client/client02.png alt=image></a></li>
                     <li><a href="#"><img src=images/client/client03.png alt=image></a></li>
                     <li><a href="#"><img src=images/client/client04.png alt=image></a></li>
                     <li><a href="#"><img src=images/client/client02.png alt=image></a></li>
                     <li><a href="#"><img src=images/client/client05.png alt=image></a></li>
                  </ul>
               </div>
            </div>
         </section>
          <footer>
            
            <div class=footer-type02>
               <div class=container>
                  <div class=row>
                     <a href=index.php class=footer-logo><img src=images/logo.png alt=image></a>
                     <div class=footer-container>
                        <ul class=clearfix>
                           <li><a href=https://www.facebook.com/ class=bigsocial-link><i class="fa fa-facebook"></i></a></li>
                           <li><a href=https://twitter.com/0effortthemes class=bigsocial-link target=_blank><i class="fa fa-twitter"></i></a></li>
                           <li><a href=https://www.behance.net/itobuz class=bigsocial-link target=_blank><i class="fa fa-behance"></i></a></li>
                        </ul>
                        <p>Copyright, 2019 teamrebount.com. Designed by <a href="https://tahcom.com/" class=copyright target="_blank" >Tahcom</a></p>
                     </div>
                     <div class=footer-appstore>
                        <figure><a href=#><img src=images/appstore/apple.png alt=image></a></figure>
                        <figure><a href=#><img src=images/appstore/google.png alt=image></a></figure>
                     </div>
                  </div>
               </div>
            </div>
         </footer>
      </div>
      <script src=js/vendor/vendor.js></script><script src=js/main.js></script>
   </body>
   <!-- Mirrored from 0effortthemes.com/soccerclubv2/player_details.php by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 28 May 2019 12:00:29 GMT -->
</html>