<!DOCTYPE html>
<?php include("../connection.php"); ?>
<?php
if(!isset($_SESSION['admin']))
{

  header("location: ../dash/log.php");
}
?>
<html class="loading" lang="en" data-textdirection="ltr">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content=".">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <title>Team Rebound</title>
    <link rel="apple-touch-icon" href="app-assets/images/logo/logo.png">
     <link rel="shortcut icon" href=../favicon.ico>
    <link href="https://fonts.googleapis.com/css?family=Muli:300,300i,400,400i,600,600i,700,700i%7CComfortaa:300,400,700" rel="stylesheet">
    <link href="app-assets/fonts/line-awesome/css/line-awesome.min.css" rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/forms/toggle/switchery.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/forms/switch.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/colors/palette-switch.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/charts/chartist.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/charts/chartist-plugin-tooltip.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN CHAMELEON  CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/app.min.css">
    <!-- END CHAMELEON  CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/vertical-menu.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/colors/palette-gradient.min.css">
    <!-- <link rel="stylesheet" type="text/css" href="app-assets/css/core/colors/palette-gradient.min.css"> -->
    <link rel="stylesheet" type="text/css" href="app-assets/css/pages/chat-application.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/pages/dashboard-analytics.min.css">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <!-- END Custom CSS-->
</head>

<body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-color="bg-gradient-x-purple-blue" data-col="2-columns">

   <nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-semi-light">
        <div class="navbar-wrapper">
            <div class="navbar-container content">
                <div class="collapse navbar-collapse show" id="navbar-mobile">
                    <ul class="nav navbar-nav mr-auto float-left">
                        <li class="nav-item mobile-menu d-md-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
                        <li class="nav-item d-none d-md-block"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu"></i></a></li>
                    </ul>
                    <ul class="nav navbar-nav float-right">



                        <li class="dropdown dropdown-user nav-item">
                            <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown"> <span class="avatar avatar-online"><img src="1.jpg" alt="avatar"></span></a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <div class="arrow_box_right"><a class="dropdown-item" href="#"><span class="avatar avatar-online"><img src="../images/logo.png" alt="avatar"><span class="user-name text-bold-700 ml-1">Admin</span></span></a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="pass.php"><i class="ft-unlock"></i> Change Password</a>
                                    <div class="dropdown-divider"></div><a class="dropdown-item" href="../dash/log.php"><i class="ft-power"></i> Logout</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>

    <div class="main-menu menu-fixed menu-light menu-accordion    menu-shadow " data-scroll-to-active="true" data-img="app-assets/images/backgrounds/02.jpg">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mr-auto">
                    <a class="navbar-brand" href="index.php"><img class="brand-logo" alt="Chameleon admin logo" src="../images/logo.png" />
                        <h3 class="brand-text">Team Rebound</h3>
                    </a>
                </li>
                <li class="nav-item d-md-none"><a class="nav-link close-navbar"><i class="ft-x"></i></a></li>
            </ul>
        </div>
        <div class="main-menu-content">
            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                 <li class="nav-item "><a href="index.php"><i class="ft-image"></i><span class="menu-title" data-i18n="">Home</span></a>
                </li>
                  <!--   <ul class="menu-content">
                        <li class="nav-item active"><a href="banner.php"><span class="menu-title" data-i18n="">Banner</span></a>
                        </li>
                        <li class="nav-item "><a href="home-about-welcome.php"><span class="menu-title" data-i18n="">Home welcome </span></a>
                        </li>
                        <li class="nav-item "><a href="specialization.php"><span class="menu-title" data-i18n="">Specialization</span></a>
                        </li>
                        
                         <li class="nav-item "><a href="latest-projects.php"><span class="menu-title" data-i18n="">Latest Projects</span></a>
                        </li>
                        
                         
                         <li class="nav-item "><a href="features.php"><span class="menu-title" data-i18n="">Features</span></a>
                        </li>
                        
                        
                             
                         <li class="nav-item "><a href="testimonials.php"><span class="menu-title" data-i18n="">Testimonials</span></a>
                        </li>



                    </ul> -->
                </li>
                <li class="nav-item "><a href="#"><i class="ft-info"></i><span class="menu-title" data-i18n="">View </span></a>
                
                  <ul class="menu-content">
                     <li class="nav-item "><a href="member.php"><span class="menu-title" data-i18n="">   Members</span></a>
                        </li>
                        
                        <li class="nav-item "><a href="public.php"><span class="menu-title" data-i18n="">participants</span></a>
                        </li>
                        
                        
                         <!--  <li class="nav-item "><a href="areas-of-work.php"><span class="menu-title" data-i18n="">Areas Of Work</span></a>
                        </li> -->



                  </ul>
                
                </li>

<!-- 
                <li class="nav-item "><a href="project-detail.php"><i class="ft-image"></i><span class="menu-title" data-i18n="">Projects</span></a>
                </li>
                <li class="nav-item "><a href="services.php"><i class="ft-disc"></i><span class="menu-title" data-i18n="">Services</span></a>
                </li>
                
                <li class="nav-item "><a href="contact.php"><i class="ft-phone"></i><span class="menu-title" data-i18n="">Contact Us</span></a>
                </li> -->
                
            </ul>
        </div>
        <div class="navigation-background"></div>
    </div>

    <div class="app-content content">

        <div class="content-wrapper">
            <div class="content-wrapper-before"></div>



            <div class="row match-height">
              
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title" id="basic-layout-form"> Change Password</h4>
                            <a class="heading-elements-toggle">
                                <i class="la la-ellipsis-v font-medium-3"></i>
                            </a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li>
                                        <a data-action="collapse">
                                            <i class="ft-minus"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a data-action="reload">
                                            <i class="ft-rotate-cw"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a data-action="expand">
                                            <i class="ft-maximize"></i>
                                        </a>
                                    </li>

                                </u l>
                            </div>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                           <form class="form-horizontal form-label-left input_mask" method="post" enctype="multipart/form-data">
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-12">

                                             
                                              
                                                    

<div class="form-group">
                                                    <label for="t2">Current Password</label>
<input type="password" id="t1" class="form-control" placeholder=" Current Password" name="t1" >
                                                </div>
                                             
                                                
<div class="form-group">
                                                    <label for="t3"> New Password</label>

                                                    <input type="password" id="t2" class="form-control" placeholder="New Password" name="t2" >
                                                </div>
                                                
   
                                                <div class="form-actions">

                                                    <input  name="b1" type="submit" class="btn btn-primary" value="Update">
                                                               
                                                </div>
                                            </div>

                                        </div>

                                    </div>


                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

 <?php
if(isset($_POST['b1']))
{

$d=date("d/m/y");

$t1=$_POST['t1'];
$t2=$_POST['t2'];

$o=md5($t1);
$n=md5($t2);


//echo "insert into  tbl_services values('$cover' ,$t1','$t2','$t3','$d','0')";

$res=mysql_query("UPDATE `tbl_log` SET `password` = '$n' where password='$o'");



    



if(mysql_affected_rows()>0)
{
msg("Successfully Uploaded","pass.php");
}
else
{
msg("Error","pass.php");

}
}










     
?>
          





        </div>













    </div>







<footer class="footer footer-static footer-light navbar-border navbar-shadow">
        <div class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2"><span class="float-md-left d-block d-md-inline-block">2018  &copy; Copyright Team Rebound . All right Reserved. Developed by <a class="text-bold-800 grey darken-2" href="https://tahcom.com/" target="_blank">Tahcom</a></span>

        </div>
    </footer>


    <!-- Modal -->
   
    <!-- Modal -->
    <div class="modal fade text-left" id="iconForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel34" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="myModalLabel34">Update Banner</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form>
                    <div class="modal-body">

                        <fieldset class="form-group">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="inputGroupFile01">
                                <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                            </div>
                        </fieldset>

                        <label>Sub Heading</label>
                        <div class="form-group position-relative has-icon-left">
                            <input type="text" placeholder="Sub Heading" class="form-control">
                            <div class="form-control-position">
                                <i class="la la-edit font-medium-5 line-height-1 text-muted icon-align"></i>
                            </div>
                        </div>

                        <label>Main Heading</label>
                        <div class="form-group position-relative has-icon-left">
                            <input type="text" placeholder="Main Heading" class="form-control">
                            <div class="form-control-position">
                                <i class="la la-edit font-medium-5 line-height-1 text-muted icon-align"></i>
                            </div>
                        </div>
                        
                        
                        
                        <label>Link</label>
                        <div class="form-group position-relative has-icon-left">
                            <input type="text" placeholder="Link" class="form-control">
                            <div class="form-control-position">
                                <i class="la la-edit font-medium-5 line-height-1 text-muted icon-align"></i>
                            </div>
                        </div>
                        
                        

                    </div>
                    <div class="modal-footer">
                        <!-- <input type="reset" class="btn btn-secondary " data-dismiss="modal" value="close"> -->
                        <input type="submit" class="btn btn-primary " value="Update">
                    </div>
                </form>
            </div>
        </div>
    </div>



    <!-- BEGIN VENDOR JS-->
    <script src="app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/forms/toggle/switchery.min.js" type="text/javascript"></script>
    <script src="app-assets/js/scripts/forms/switch.min.js" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="app-assets/vendors/js/charts/chartist.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/charts/chartist-plugin-tooltip.min.js" type="text/javascript"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN CHAMELEON  JS-->
    <script src="app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="app-assets/js/core/app.min.js" type="text/javascript"></script>
    <script src="app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/jquery.sharrre.js" type="text/javascript"></script>
    <!-- END CHAMELEON  JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="app-assets/js/scripts/pages/dashboard-analytics.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS-->
</body>

</html>