<!doctype html>
<?php include("connection.php"); ?>
<?php
if(!isset($_SESSION['muser']))
{

  header("location: login.php");
}
?>
<html class=no-js lang="">
   <!-- Mirrored from 0effortthemes.com/soccerclubv2/blog.php by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 28 May 2019 11:59:29 GMT -->
   <!-- Added by HTTrack -->
   <meta http-equiv="content-type" content="text/html;charset=utf-8" />
   <!-- /Added by HTTrack -->
   <head>
      <meta charset=utf-8>
      <meta name=description content="">
      <meta name=viewport content="width=device-width, initial-scale=1">
      <title>Team Rebound</title>
      <link rel="shortcut icon" href=favicon.ico>
      <link rel=stylesheet href=vendor.css>
      <link rel=stylesheet href=style.css>
      <link rel=stylesheet type=text/css href=css/layerslider.css>
      <link rel="stylesheet" href="css/jquery.dateselect.css">
       <link rel="stylesheet" href="css/newtable.css">
       <link rel="stylesheet" href="css/profile.css">
       <link rel="stylesheet" href="css/plugin.css">
       <link rel="stylesheet" href="css/color.css">
       <link rel="stylesheet" href="css/flaticon.css">
       
      <script src=js/vendor/modernizr.js></script>
   </head>
   <body>
      <!--[if lt IE 10]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
      <![endif]-->
      <div class=wrapper>
         <header class=header-main>
            
            <div class="header-lower clearfix">
               <div class=container>
                  <div class=row>
                     <h1 class=logo><a href=index.php><img src=images/logo.png alt=image></a></h1>
                     <div class=menubar >
                        <nav class=navbar>
                           <div class=nav-wrapper>
                              <div class=navbar-header><button type=button class=navbar-toggle><span class=sr-only>Toggle navigation</span> <span class=icon-bar></span></button></div>
                              <div class=nav-menu>
                                 <ul class="nav navbar-nav menu-bar">
                                    <li><a href=profile.php class="active">Home</a></li>
                                    <li><a href=directory.php >Directory</a></li>
                                    <li><a href=out.php >logout</a></li>
                                 </ul>
                              </div>
                           </div>
                        </nav>
                     </div>
                     <div class=social><a href=https://www.facebook.com/ class=facebook><i class="fa fa-facebook"></i></a> <a href=https://twitter.com/ class=twitter><i class="fa fa-twitter"></i></a></div>
                  </div>
               </div>
            </div>
         </header>
        
<div class="candidate-detail-two-subheader">
            <span class="candidate-detail-two-transparent"></span>
            <div class="container">
                <div class="row">
                  <?php
        $user=$_SESSION['muser'];
$res=mysql_query("select * from tbl_reg where username='$user'");
$row=mysql_fetch_row($res);

        ?>
                    <div class="candidate-detail-two-subheaderwrap" style="z-index: 99998; position: relative;">
                        <h1><?php echo $row[0]; ?></h1>
                        <ul class="candidate-detail-two-subheader-list">
                            <li>Location: <?php echo $row[1]; ?></li>
                            <li><i class="fa fa-map-marker"></i> <?php echo $row[9]; ?></li>
                            <li><i class="careerfy-icon careerfy-calendar"></i> Member Since, <?php echo $row[12]; ?></li>
                        </ul>
                        <a href="profileedit.php" class="candidate-detail-two-subheader-btn" style="z-index: 99999; position: relative;">Edit Profile</a>
                        <ul class="candidate-detail-two-subheader-social">
                          <?php
$trid=$_SESSION['reboundid'];
$res1=mysql_query("select * from tbl_social where reboundid='$trid'");
$row1=mysql_fetch_row($res1);

?>

                            <li><a href="<?php echo $row1[1]; ?>" class="fa fa-facebook"></a></li>
                            <li><a href="<?php echo $row1[2]; ?>" class="fa fa-twitter"></a></li>
                            <li><a href="<?php echo $row1[3]; ?>" class="fa fa-linkedin"></a></li>
                            
                        </ul>
                    </div>
                 
                </div>
            </div>
        </div>
      
        <div class="careerfy-main-content">
            
            <!-- Main Section -->
            <div class="careerfy-main-section">
                <div class="container">
                    <div class="row">

                        <aside class="careerfy-column-4">
                            <div class="careerfy-typo-wrap">
                                <div class="candidate-detail-two-thumb">
                                    <img src="profilephoto/<?php echo $row[8]; ?>" alt="">
                                </div>
                                <div class="widget widget_contact_form">
                                <div class="careerfy-widget-title"><h2>Basic info</h2></div>
                                    <form>
                                        <ul>
                                            <li><label>Phone: <?php echo $row[3]; ?></label></li>
                                            <li><label>Email: <?php echo $row[2]; ?></label></li>
                                            
                                        </ul>
                                    </form>
                                </div>
                            </div>
                        </aside>
                        <div class="careerfy-column-8">
                            <div class="careerfy-typo-wrap">
                                <div class="careerfy-candidate-editor">
                                    <div class="careerfy-content-title"><h2>About me</h2></div>
                                    
                                    <div class="careerfy-description">
                                        <p><?php echo $row[10]; ?></p>
                                    </div>

                                </div>
                                <div class="careerfy-candidate-title"> <h2><i class="careerfy-icon careerfy-mortarboard"></i> Career History</h2> </div>
                                <div class="careerfy-candidate-timeline-two">
                                    <ul class="careerfy-row">

                                      <?php

$res2=mysql_query("select * from tbl_club where reboundid='$trid'");
while($row2=mysql_fetch_row($res2))
{
                                      ?>
                                        <li class="careerfy-column-12">
                                            <div class="careerfy-candidate-timeline-two-text">
                                                <span>Club: <?php echo $row2[0]; ?> <small><?php echo $row2[1]; ?></small></span>
                                                <p><?php echo $row2[2]; ?></p>
                                            </div>
                                        </li>
                                      <?php } ?>
                                      
                                    </ul>
                                </div>
                                
                                
                                
                                
                                
                               
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- Main Section -->
        </div>



         
          <footer>
            
            <div class=footer-type02>
               <div class=container>
                  <div class=row>
                     <a href=index.php class=footer-logo><img src=images/logo.png alt=image></a>
                     <div class=footer-container>
                        <ul class=clearfix>
                           <li><a href=https://www.facebook.com/ class=bigsocial-link><i class="fa fa-facebook"></i></a></li>
                           <li><a href=https://twitter.com/0effortthemes class=bigsocial-link target=_blank><i class="fa fa-twitter"></i></a></li>
                          
                        </ul>
                        <p>Copyright, 2019 teamrebount.com. Designed by <a href="https://tahcom.com/" class=copyright target="_blank" >Tahcom</a></p>
                     </div>
                     <div class=footer-appstore>
                        <figure><a href=#><img src=images/appstore/apple.png alt=image></a></figure>
                        <figure><a href=#><img src=images/appstore/google.png alt=image></a></figure>
                     </div>
                  </div>
               </div>
            </div>
         </footer>
      </div>
      <script src=js/vendor/vendor.js></script><script src=js/main.js></script>
      <script type="text/javascript" src="js/jquery.dateselect.js"></script>

   </body>
   <!-- Mirrored from 0effortthemes.com/soccerclubv2/blog.php by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 28 May 2019 11:59:50 GMT -->
</html>